terraform {
  backend "s3" {
    bucket = "bucket-for-tf-ws"
    key    = "vpc/state"
    region = "us-east-1"
//    dynamodb_table = "terraform-state-lock-dynamo"
  }
}


//{
//  "Version": "2012-10-17",
//"Statement": [
//{
//"Effect": "Allow",
//"Action": "s3:ListBucket",
//"Resource": "arn:aws:s3:::bucket-for-tf-ws"
//},
//{
//"Effect": "Allow",
//"Action": ["s3:GetObject", "s3:PutObject"],
//"Resource": "arn:aws:s3:::bucket-for-tf-ws/vpc/state"
//}
//]
//}

//resource "aws_dynamodb_table" "dynamodb-terraform-state-lock" {
//  name = "terraform-state-lock-dynamo"
//  hash_key = "LockID"
//  read_capacity = 20
//  write_capacity = 20
//
//  attribute {
//    name = "LockID"
//    type = "S"
//  }
//
//  tags {
//    Name = "DynamoDB Terraform State Lock Table"
//  }
//}




variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

variable "availability_zones" {
  type        = "list"
  default     = ["us-east-1a", "us-east-1b", "us-east-1c", "us-east-1d", "us-east-1e", "us-east-1f"]
  description = "List of Availability Zones"
}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_vpc" "my_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags {
    Name = "VPC-DEMO-${terraform.workspace}"
  }
}

resource "aws_subnet" "public" {
  count                   = 6
  vpc_id                  = "${aws_vpc.my_vpc.id}"
  cidr_block              = "10.0.${count.index+1}.0/24"
  availability_zone       = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true

  tags {
    Name = "VPC-DEMO-${terraform.workspace} Public Subnet ${count.index+1}"
  }
}

resource "aws_subnet" "private" {
  count             = 6
  vpc_id            = "${aws_vpc.my_vpc.id}"
  cidr_block        = "10.0.${count.index+11}.0/24"
  availability_zone = "${element(var.availability_zones, count.index)}"

  tags {
    Name = "VPC-DEMO-${terraform.workspace} Private Subnet ${count.index+1}"
  }
}

//----------------------------------------------------------------------
//        Create GW

resource "aws_internet_gateway" "my_vpc_igw" {
  vpc_id = "${aws_vpc.my_vpc.id}"

  tags {
    Name = "VPC-DEMO-${terraform.workspace} Internet Gateway"
  }
}

//------------------------------------------------------------------------
//        Create RT for subnets

resource "aws_route_table" "rtDEMO" {
  vpc_id = "${aws_vpc.my_vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.my_vpc_igw.id}"
  }

  tags {
    Name = "VPC-DEMO-${terraform.workspace} Public Subnet Route Table"
  }
}

resource "aws_route_table_association" "public" {
  count          = 6
  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.rtDEMO.id}"
}
