terraform {
  backend "s3" {
    bucket = "bucket-for-tf-ws"
    key    = "app/state"
    region = "us-east-1"

    //    dynamodb_table = "terraform-state-lock-dynamo"
  }
}

variable "aws_region" {
  description = "The AWS region to create things in."
  default     = "us-east-1"
}

data "aws_vpc" "selected" {
  tags {
    Name = "VPC-DEMO-${terraform.workspace}"
  }
}

//data "aws_subnet" "selected" {
//  tags {
//    Name = "VPC-DEMO-${terraform.workspace} Public Subnet 2"
//  }
//}

data "aws_subnet_ids" "all" {
  vpc_id = "${data.aws_vpc.selected.id}"

  tags {
    Name = "VPC-DEMO-${terraform.workspace} Public Subnet *"
  }
}

provider "aws" {
  region = "${var.aws_region}"
}

resource "aws_s3_bucket" "b" {
  bucket = "kss-tf-demo-bucket"
  acl    = "private"

  provisioner "local-exec" {
    command = "aws s3 cp default s3://kss-tf-demo-bucket/default"
  }

  tags = {
    Name = "nginx-configuration"
  }
}

//resource "aws_s3_bucket_object" "folder1" {
//  bucket = "${aws_s3_bucket.b.id}"
//  acl    = "private"
//  key    = "default"
//  source = "default"
//}

resource "aws_iam_instance_profile" "instance_profile" {
  name = "instance_profile_${terraform.workspace}"
  role = "${aws_iam_role.s3_access_role.name}"
}

resource "aws_iam_role_policy" "s3_iam_role_policy" {
  name = "s3_iam_role_policy"
  role = "${aws_iam_role.s3_access_role.id}"

  policy = <<EOF
{
      "Version": "2012-10-17",
      "Id" : "S3BUCKETPOL",
      "Statement": [
        {
          "Effect": "Allow",
          "Action": [
          "s3:Get*",
          "s3:List*"
          ],
          "Resource": "arn:aws:s3:::kss-tf-demo-bucket/*"
        }
      ]
}
EOF
}

resource "aws_iam_role" "s3_access_role" {
  name = "s3_access_role-${terraform.workspace}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF

  tags = {
    Name = "s3_access_role-${terraform.workspace}"
  }
}

resource "aws_security_group" "sg-ec2" {
  name        = "SG-EC2-${terraform.workspace}"
  description = "Allow traffic to EC2"
  vpc_id      = "${data.aws_vpc.selected.id}"

  ingress {
    description = "Allow inbound SSH traffic from any IP"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description     = "Allow all from LB"
    from_port       = 0
    protocol        = "-1"
    to_port         = 0
    security_groups = ["${aws_security_group.sg-elb.id}"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SG-EC2-${terraform.workspace}"
  }
}

resource "aws_security_group" "sg-elb" {
  name        = "SG-ELB-${terraform.workspace}"
  description = "Allow traffic to ELB"
  vpc_id      = "${data.aws_vpc.selected.id}"

  ingress {
    description = "Allow inbound SSH traffic from any IP"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    protocol    = "-1"
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "SG-ELB-${terraform.workspace}"
  }
}

data "aws_ami" "ubuntu" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-bionic-18.04-amd64-server-*"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_elb" "clb" {
  name = "demo-app-01-${terraform.workspace}"

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 3
    timeout             = 5
    target              = "HTTP:80/"
    interval            = 30
  }

  //  instances                   = ["${aws_instance.web.id}"]
  cross_zone_load_balancing   = true
  idle_timeout                = 400
  connection_draining         = true
  connection_draining_timeout = 400
  security_groups             = ["${aws_security_group.sg-elb.id}"]
  subnets                     = ["${data.aws_subnet_ids.all.ids}"]

  tags = {
    Name = "demo-app-01-${terraform.workspace}"
  }
}

resource "aws_launch_configuration" "demo" {
  name_prefix                 = "lc-demo-${terraform.workspace}"
  image_id                    = "${data.aws_ami.ubuntu.id}"
  instance_type               = "t2.micro"
  security_groups             = ["${aws_security_group.sg-ec2.id}"]
  key_name                    = "nvirginia"
  associate_public_ip_address = "true"
  iam_instance_profile        = "${aws_iam_instance_profile.instance_profile.name}"

  user_data = <<-EOF
              #!/bin/bash
              sudo apt-get update
              sudo apt-get -y install nginx awscli php-fpm
              sudo aws s3 cp s3://kss-tf-demo-bucket/default /etc/nginx/sites-available/default
              sudo systemctl restart nginx
              sudo bash -c 'echo "Hello, World from <b STYLE=\"color:blue;\">${terraform.workspace}</b></i> <p>\
              <a href=\"info.php\">Info</a>" > /var/www/html/index.html'
              sudo bash -c 'echo "<?php phpinfo(); ?>" > /var/www/html/info.php'
              EOF

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "demo" {
  name                 = "asg-demo-${terraform.workspace}"
  launch_configuration = "${aws_launch_configuration.demo.id}"

  vpc_zone_identifier = ["${data.aws_subnet_ids.all.ids}"]
  min_size            = 1
  max_size            = 1
  load_balancers      = ["${aws_elb.clb.name}"]
  health_check_type   = "ELB"
  desired_capacity    = 1

  tag {
    key                 = "Name"
    value               = "demo-app-01-${terraform.workspace}"
    propagate_at_launch = true
  }
}
